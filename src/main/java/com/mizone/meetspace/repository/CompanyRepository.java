package com.mizone.meetspace.repository;

import com.mizone.meetspace.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company ,Integer> {
}
