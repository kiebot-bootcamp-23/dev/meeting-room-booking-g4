package com.mizone.meetspace.repository;

import com.mizone.meetspace.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
