package com.mizone.meetspace.repository;

import com.mizone.meetspace.model.Slot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

@Repository
public interface SlotsRepository extends JpaRepository<Slot,Integer> {

    @Query(value = "select * from slot s left join booking_slot bs on b.id=bs.slot_id join booking b on bs.booking_id=b.id and b.date=:date and b.meeting_room_id=:roomId and bs.slot_id=null",nativeQuery = true)
    List<Slot> getAvailableSlots(Integer roomId, LocalDate date);
}
