package com.mizone.meetspace.repository;

import com.mizone.meetspace.model.MeetingRoom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MeetingRoomRepository extends JpaRepository<MeetingRoom,Integer> {
}
