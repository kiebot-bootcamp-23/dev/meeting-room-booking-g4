package com.mizone.meetspace.repository;

import com.mizone.meetspace.model.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BookingRepository extends JpaRepository<Booking,Integer> {

    @Query(value = "select * from booking b where b.company_id=:id and b.is_active=true",nativeQuery = true)
    List<Booking> findBookingDetailsOfCompanies(Integer id);

    @Query(value = "select * from booking b where b.is_active=true",nativeQuery = true)
    List<Booking> findActiveBookings();
}
