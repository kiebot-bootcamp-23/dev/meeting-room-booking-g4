package com.mizone.meetspace.controller;

import com.mizone.meetspace.VM.BookingVM;
import com.mizone.meetspace.model.Booking;
import com.mizone.meetspace.model.BookingSlot;
import com.mizone.meetspace.model.Slot;
import com.mizone.meetspace.repository.BookingRepository;
import com.mizone.meetspace.repository.BookingSlotRepository;
import com.mizone.meetspace.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
public class BookingController {

    @Autowired
    BookingService bookingService;
    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private BookingSlotRepository bookingSlotRepository;

    @GetMapping("/bookings")
    public List<Booking> getBookingDetails() {
        return bookingRepository.findActiveBookings();
    }

    @PostMapping("/bookings")
    public ResponseEntity bookMeetingRoom(@RequestBody BookingVM bookingVM) {
        List<Booking> previousBookings = bookingRepository.findBookingDetailsOfCompanies(bookingVM.getBooking().getCompany().getId());
        if (previousBookings.size() > 0) {
            return ResponseEntity.badRequest().body("booking already exist");
        } else {
            bookingVM.getBooking().setTotalHours(bookingService.getTotalHours(bookingVM.getSlots()));
            bookingVM.getBooking().setFinalPrice(bookingService.calculatePrice(1200, bookingVM.getSlots()));
            bookingVM.getBooking().setActive(true);
            bookingVM.getBooking().setBookedDate(LocalDate.now());
            Booking booking = bookingRepository.save(bookingVM.getBooking());
            for (Slot slot : bookingVM.getSlots()) {
                bookingSlotRepository.save(new BookingSlot(booking, slot));
            }
            return ResponseEntity.ok().body(bookingRepository.save(booking));
        }
    }

    @GetMapping("bookings/{id}/cancel")
    public ResponseEntity cancelBooking(@PathVariable Integer id) {
        Optional<Booking> booking = bookingRepository.findById(id);
        if (booking.isEmpty()) {
            return ResponseEntity.badRequest().body("No booking present for the id");
        }
        booking.get().setActive(false);
        booking.get().setCancelledDate(LocalDate.now());
        return ResponseEntity.ok().body("Booking Cancelled Successfully");
    }

    @GetMapping("bookings/{id}/details")
    public ResponseEntity getBookingDetailsByCompanyId(@PathVariable Integer id) {
        return ResponseEntity.ok().body(bookingRepository.findBookingDetailsOfCompanies(id));
    }
}

