package com.mizone.meetspace.controller;


import com.mizone.meetspace.model.Company;
import com.mizone.meetspace.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;


@RestController
public class CompanyController {

    @Autowired
    CompanyRepository companyRepository;

    @PostMapping("/companies")
    ResponseEntity createCompany(@Validated @RequestBody Company company, UriComponentsBuilder uriComponentsBuilder){
        final Company createdCompany = companyRepository.save(company);
        final URI location = uriComponentsBuilder.path("/api/companies").buildAndExpand(createdCompany.getId()).toUri();
        return ResponseEntity.created(location).body(createdCompany);
    }


    @GetMapping("/companies")
    List<Company> getAllCompanies(){
        return companyRepository.findAll();
    }

    @GetMapping("/companies/{id}")
    ResponseEntity getOneCompany(@PathVariable("id") Integer id){
        Optional<Company> company = companyRepository.findById(id);
        if(company.isEmpty()){
            ResponseEntity.badRequest().body("Company does not exist");
        }
        return ResponseEntity.ok().body(company);
    }


    @PutMapping("/companies/{id}")
    ResponseEntity updateCompany(@PathVariable("id") Integer id,@RequestBody Company company){
        Optional<Company> existingCompany = companyRepository.findById(id);
        if(existingCompany.isEmpty()){
            return ResponseEntity.badRequest().body("Invalid company id "+id);
        }
        Company updateCompany = existingCompany.get();
        updateCompany.setName(company.getName());
        return ResponseEntity.ok().body(companyRepository.save(updateCompany));
    }

    @DeleteMapping("/companies/{id}")
    ResponseEntity deleteCompany(@PathVariable("id") Integer id) {
        companyRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
