package com.mizone.meetspace.controller;

import com.mizone.meetspace.model.Slot;
import com.mizone.meetspace.repository.SlotsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
public class SlotsController {
    @Autowired
    SlotsRepository slotsRepository;

    @GetMapping("/slots")
    public ResponseEntity<List<Slot>> getAllSlots() {
        return ResponseEntity.ok().body(slotsRepository.findAll());
    }

    @GetMapping("/slots/{id}")
    public ResponseEntity<Slot> getSlot(@PathVariable Integer id) {
        Optional<Slot> slot = slotsRepository.findById(id);
        if (slot.isEmpty()) {
            ResponseEntity.badRequest().body("Invalid id " + id);
        }
        return ResponseEntity.ok().body(slot.get());
    }

    @DeleteMapping("/slots/{id}")
    public ResponseEntity deleteSlot(@PathVariable Integer id) {
        if (slotsRepository.existsById(id)) {
            slotsRepository.deleteById(id);
            return ResponseEntity.ok().body("Deleted successfully");
        }
        return ResponseEntity.badRequest().body("Invalid id " + id);
    }

    @PostMapping("/slots")
    public ResponseEntity<Slot> createSlot(@RequestBody Slot slot) {
        Slot savedSlot = slotsRepository.save(slot);
        return ResponseEntity.ok().body(savedSlot);
    }

    @PutMapping("/slots/{id}")
    public ResponseEntity updateSlot(@PathVariable Integer id, @RequestBody Slot slot) {
        if (!slotsRepository.existsById(id)) {
            return ResponseEntity.badRequest().body("Invalid id " + id);
        } else {
            slot.setId(id);
            Slot savedSlot = slotsRepository.save(slot);
            return ResponseEntity.ok().body(savedSlot);
        }
    }

    @GetMapping("/slots/{roomId}/available-slots")
    public List<Slot> getAvailableSlots(@PathVariable Integer roomId, @RequestParam String date){
        return slotsRepository.getAvailableSlots(roomId,LocalDate.parse(date));
    }

}
