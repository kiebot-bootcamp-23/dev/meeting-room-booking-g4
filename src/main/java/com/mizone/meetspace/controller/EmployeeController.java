package com.mizone.meetspace.controller;

import com.mizone.meetspace.model.Company;
import com.mizone.meetspace.model.Employee;
import com.mizone.meetspace.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class EmployeeController {

    @Autowired
    EmployeeRepository employeeRepository;

    @GetMapping("/employees")
    List<Employee> getAllEmployees(){
        return employeeRepository.findAll();
    }

    @GetMapping("/employees/{id}")
    ResponseEntity getEmployee(@PathVariable("id") Integer id){
        Optional<Employee> employee = employeeRepository.findById(id);
        if(employee.isEmpty()){
            ResponseEntity.badRequest().body("Employee does not exist");
        }
        return ResponseEntity.ok().body(employee);
    }
    @PostMapping("/employees")
    ResponseEntity createAnEmployee(@RequestBody Employee employee) {
        Employee createdEmployee = employeeRepository.save(employee);
        return ResponseEntity.ok().body(createdEmployee);
    }

    @DeleteMapping("employees/{id}")
    ResponseEntity deleteEmployeeById(@PathVariable("id") Integer id) {
        if (employeeRepository.existsById(id)) {
            employeeRepository.deleteById(id);
            return ResponseEntity.ok().body("Deleted successfully");
        }
        return ResponseEntity.badRequest().body("Inalid id");
    }

    @PutMapping("employees/{id}")
    ResponseEntity editEmployeeById(@PathVariable("id") Integer id, @RequestBody Employee employee) {
        Optional<Employee> existingEmployee = employeeRepository.findById(id);
        if (existingEmployee.isEmpty()) {
            return ResponseEntity.badRequest().body("Employee does not exist");
        }
        employee.setId(id);
        Employee savedEmployee = employeeRepository.save(employee);
        return ResponseEntity.ok().body(savedEmployee);
    }

}
