package com.mizone.meetspace.controller;

import com.mizone.meetspace.model.MeetingRoom;
import com.mizone.meetspace.repository.MeetingRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class MeetingRoomController {
    @Autowired
    MeetingRoomRepository meetingRoomRepository;

    @GetMapping("/meetingrooms")
    public ResponseEntity<List<MeetingRoom>> getAllMeetingRooms() {
        return ResponseEntity.ok().body(meetingRoomRepository.findAll());
    }

    @GetMapping("/meetingrooms/{id}")
    public ResponseEntity<MeetingRoom> getAllMeetingRooms(@PathVariable Integer id) {
        Optional<MeetingRoom> meetingRoom = meetingRoomRepository.findById(id);
        if (meetingRoom.isEmpty()) {
            ResponseEntity.badRequest().body("Invalid id " + id);
        }
        return ResponseEntity.ok().body(meetingRoom.get());
    }

    @DeleteMapping("/meetingrooms/{id}")
    public ResponseEntity deleteMeetingRoom(@PathVariable Integer id) {
        Optional<MeetingRoom> meetingRoom = meetingRoomRepository.findById(id);
        if (meetingRoom.isEmpty()) {
            return ResponseEntity.badRequest().body("Invalid id " + id);
        }
        else {
            meetingRoomRepository.deleteById(id);
            return ResponseEntity.ok().body("deleted successfully");
        }
    }

    @PostMapping("/meetingrooms")
    public ResponseEntity<MeetingRoom> createMeetingRoom(@RequestBody MeetingRoom meetingRoom) {
        MeetingRoom savedMeetingRoom = meetingRoomRepository.save(meetingRoom);
        return ResponseEntity.ok().body(savedMeetingRoom);
    }

    @PutMapping("/meetingrooms/{id}")
    public ResponseEntity updateMeetingRoom(@PathVariable Integer id, @RequestBody MeetingRoom meetingRoom) {
        Optional<MeetingRoom> existingMeetingRoom = meetingRoomRepository.findById(id);
        if (existingMeetingRoom.isEmpty()) {
            return ResponseEntity.badRequest().body("Invalid id " + id);
        } else {
            meetingRoom.setId(id);
            MeetingRoom savedMeetingRoom = meetingRoomRepository.save(meetingRoom);
            return ResponseEntity.ok().body(savedMeetingRoom);
        }
    }
}


