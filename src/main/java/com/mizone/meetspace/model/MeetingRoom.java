package com.mizone.meetspace.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class MeetingRoom {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;

    private boolean hasAc;
    private Integer pricePerHour;
    private Integer capacity;

    public MeetingRoom() {
    }

    public MeetingRoom(Integer id, String name, boolean hasAc, Integer pricePerHour, Integer capacity) {
        this.id = id;
        this.name = name;
        this.hasAc = hasAc;
        this.pricePerHour = pricePerHour;
        this.capacity = capacity;
    }

    public MeetingRoom(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasAc() {
        return hasAc;
    }

    public void setHasAc(boolean hasAc) {
        this.hasAc = hasAc;
    }

    public Integer getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(Integer pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }
}
