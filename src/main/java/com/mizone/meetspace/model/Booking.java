package com.mizone.meetspace.model;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.List;

@Entity
public class Booking {
    @Id
    @GeneratedValue
    private Integer Id;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;

    private Integer finalPrice;

    private Integer totalHours;

    private boolean isActive;

    private LocalDate bookedDate;

    private LocalDate cancelledDate;

    @ManyToOne
    @JoinColumn(name = "meeting_room_id")
    private MeetingRoom meetingRoom;

    public Booking(Integer id, Company company, Employee employee, Integer finalPrice, BookingSlot bookingSlot, Integer totalHours, boolean isActive, LocalDate bookedDate, LocalDate cancelledDate, MeetingRoom meetingRoom) {
        Id = id;
        this.company = company;
        this.employee = employee;
        this.finalPrice = finalPrice;
        this.totalHours = totalHours;
        this.isActive = isActive;
        this.bookedDate = bookedDate;
        this.cancelledDate = cancelledDate;
        this.meetingRoom = meetingRoom;
    }

    public Booking() {
    }

    public Booking(Integer id) {
        Id = id;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Integer getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Integer finalPrice) {
        this.finalPrice = finalPrice;
    }


    public Integer getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(Integer totalHours) {
        this.totalHours = totalHours;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public LocalDate getBookedDate() {
        return bookedDate;
    }

    public void setBookedDate(LocalDate bookedDate) {
        this.bookedDate = bookedDate;
    }

    public LocalDate getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(LocalDate cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public MeetingRoom getMeetingRoom() {
        return meetingRoom;
    }

    public void setMeetingRoom(MeetingRoom meetingRoom) {
        this.meetingRoom = meetingRoom;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "Id=" + Id +
                ", company=" + company +
                ", employee=" + employee +
                ", finalPrice=" + finalPrice +
                ", totalHours=" + totalHours +
                ", isActive=" + isActive +
                ", bookedDate=" + bookedDate +
                ", cancelledDate=" + cancelledDate +
                ", meetingRoom=" + meetingRoom +
                '}';
    }
}
