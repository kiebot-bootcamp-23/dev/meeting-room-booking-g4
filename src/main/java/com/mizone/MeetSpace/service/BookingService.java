package com.mizone.meetspace.service;

import com.mizone.meetspace.model.Slot;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookingService {
    public Integer calculatePrice(Integer price, List<Slot> slots){
        Integer totalSlots=slots.size();
        Integer totalPrice=0;
        totalPrice=(totalSlots*price);
        return totalPrice;
    }

    public Integer getTotalHours(List<Slot> slots){
        Integer totalSlots=slots.size();
        return totalSlots;
    }
}
