package com.mizone.meetspace.VM;

import com.mizone.meetspace.model.Booking;
import com.mizone.meetspace.model.Slot;

import java.util.List;

public class BookingVM {
    List<Slot> slots;
    Booking booking;

    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }
}
