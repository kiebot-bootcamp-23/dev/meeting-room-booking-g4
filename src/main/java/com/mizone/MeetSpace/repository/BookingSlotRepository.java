package com.mizone.meetspace.repository;

import com.mizone.meetspace.model.BookingSlot;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingSlotRepository extends JpaRepository<BookingSlot,Integer> {
}
