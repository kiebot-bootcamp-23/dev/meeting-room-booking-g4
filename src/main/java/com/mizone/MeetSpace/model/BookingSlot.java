package com.mizone.meetspace.model;

import jakarta.persistence.*;

@Entity
public class BookingSlot {
    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "booking_id")
    private Booking booking;

    @ManyToOne
    @JoinColumn(name = "slot_id")
    private Slot slot;

    public BookingSlot(Integer id, Booking booking, Slot slot) {
        this.id = id;
        this.booking = booking;
        this.slot = slot;
    }

    public BookingSlot(Booking booking, Slot slot) {
        this.booking = booking;
        this.slot = slot;
    }

    public BookingSlot() {
    }

    public BookingSlot(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }
}
