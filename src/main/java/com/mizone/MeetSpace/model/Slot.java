package com.mizone.meetspace.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class Slot {
    @Id
    @GeneratedValue
    private Integer id;

    private String slots;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSlots() {
        return slots;
    }

    public void setSlots(String slots) {
        this.slots = slots;
    }

    @Override
    public String   toString() {
        return "Slots{" +
                "id=" + id +
                ", slots='" + slots + '\'' +
                '}';
    }
}
